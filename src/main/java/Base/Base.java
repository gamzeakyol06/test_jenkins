package Base;

import io.restassured.RestAssured;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;

import java.net.MalformedURLException;

public class Base {
    protected static WebDriver driver;


    public final static String MAIN_PAGE_URL = "http://lima.eldor.com.tr";

   // public final static String MAIN_PAGE_URL = "https://pamis-webdmin.niso.dev/";

    public void beforemethod() throws MalformedURLException, InterruptedException {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setCapability("browserName","chrome");
        chromeOptions.setCapability("platformName","LINUX");
        chromeOptions.addArguments("start-maximized");
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--remote-allow-origins=*");
        chromeOptions.setCapability("acceptInsecureCerts", true);


        driver = new ChromeDriver(chromeOptions);

        System.setProperty("webdriver.chrome.driver","/src/main/driver/chromedriver");
        System.out.println(System.getProperty("webdriver.chrome.driver"));

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        System.out.println("hello git");

        //chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        // System.setProperty("javax.net.ssl.trustStore","clientTrustStore.key");
        //  System.setProperty("javax.net.ssl.trustStorePassword","qwerty");
        /*      System.out.println(System.getProperty("javax.net.ssl.trustStore"));*/
        System.setProperty("javax.net.ssl.trustStore","clientTrustStore.key");
        System.setProperty("javax.net.ssl.trustStorePassword","qwerty");

        RestAssured.useRelaxedHTTPSValidation(); // yetki problemi cozuldu
    }

    public void aftermethod(){
        driver.quit();
    }
}
